Some useful utilites are included:

- Remove Sticky -
  Hides those horisontal bars that follow you around when you scroll the page.

- Faster/Slower Video -
  Finetune the talking speed pacing a lecture to your comfort.

- Content Editable -
  You can edit and remove things from the page.

- Unload other Tabs -
  Free up memory from inactive browser tabs. Can be later reloaded.
